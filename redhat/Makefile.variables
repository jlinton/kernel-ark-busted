
# Variables below should be changed as needed for building on a different
# branch, or with a different upstream branch. This should be the only
# file that needs to be changed now, and is included in redhat/Makefile
# and redhat/Makefile.common

# This is the source branch that you are building out of typically this is
# the current branch.
DIST_BRANCH ?= "os-build"

# This is the dist release suffix used in the package release, eg. .fc34,
# .el8 etc. In a different branch this may be set to a fixed value.
DIST ?= $(shell $(RPMBUILD) --eval '%{?dist}')

# The branch used as upstream. This is what the upstream tarball is it
# should be tracked in a local branch. This would be "master" for the
# Linus master branch or linux-5.x.y for a stable branch. It can also be
# any other upstream you have added as a branch locally.
UPSTREAM_BRANCH ?= master

# If VERSION_ON_UPSTREAM is set, the versioning of the rpm package is based
# on a branch tracking upstream. This allows for generating rpms
# based on untagged releases.
VERSION_ON_UPSTREAM:=1

# RELEASED_KERNEL swaps between the pre-release secureboot keys and
# the release one, for vmlinux signing. It also controls whether Fedora
# kernels are built as debug kernels or release kernels with a separate
# kernel-debug build.
# Set RELEASED_KERNEL to 1 when the upstream source tarball contains a
#  kernel release. (This includes prepatch or "rc" releases.)
# Set RELEASED_KERNEL to 0 when the upstream source tarball contains an
#  unreleased kernel development snapshot.
RELEASED_KERNEL:=0

# BUMP_RELEASE determines whether the pkgrelease is bumped when you call
# 'make dist-release'.  It should be set to "yes" if you expect to do
# multiple updates and builds without bumping the base version, and no
# when you expect every build to use a different base kernel version.
BUMP_RELEASE:=yes

# INCLUDE_*_FILES determines whether or not the specific OS flavor files will
# be included as part of the source RPM.  Downstream projects, such as
# centos-stream or RHEL, can disable file inclusion by setting these values
# to '0'.
INCLUDE_FEDORA_FILES:=1
INCLUDE_RHEL_FILES:=1

# In case PATCHLIST_URL is not set to "none", Patchlist.changelog is added to
# the kernel src.rpm, which will contain shas and commits not upstream. The
# value of PATCHLIST_URL in this case should point to the git repository where
# the commits are located, and will be added as a prefix to the shas listed.
PATCHLIST_URL ?= "https://gitlab.com/cki-project/kernel-ark/-/commit"
